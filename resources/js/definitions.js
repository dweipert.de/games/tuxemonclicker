/**
 * @typedef {string} MonsterSlug
 * @typedef {string} TechniqueSlug
 * @typedef {string} ItemSlug
 * @typedef {string} AreaSlug
 * @typedef {string} StorySlug
 */

//

/**
 * @enum
 */
const ElementType = {
  aether: 'aether',
  wood: 'wood',
  fire: 'fire',
  earth: 'earth',
  metal: 'metal',
  water: 'water',

  /* lightning: 'lightning',
  frost: 'frost',
  venom: 'venom',
  //vermin: 'vermin',
  cosmic: 'cosmic',
  battle: 'battle',
  psionic: 'psionic',
  darkness: 'darkness',
  heaven: 'heaven',

  combineTypes(typeA, typeB) {
    if (typeA === ElementType.earth & typeB === ElementType.fire) {
      return ElementType.lightning;
    }
    if (typeA === ElementType.earth & typeB === ElementType.water) {
      return ElementType.frost;
    }
    if (typeA === ElementType.earth & typeB === ElementType.wood) {
      return ElementType.venom;
    }
    // if (typeA === ElementType.earth & typeB === ElementType.metal) {
    //   return ElementType.vermin;
    // }
    if (typeA === ElementType.fire && typeB === ElementType.water) {
      return ElementType.cosmic;
    }
  } */
};

const ElementTypeColor = {
  [ElementType.aether]: 'rgba(255, 255, 255, 1)',
  [ElementType.wood]: '#3ca6a6',
  [ElementType.fire]: '#ca3c3c',
  [ElementType.earth]: '#eac93c',
  [ElementType.metal]: '#e4e4e4',
  [ElementType.water]: '#3c3c3c',
};


/**
 * @readonly
 * @enum {string}
 */
const TasteWarm = {
  tasteless: 'tasteless',
  peppy: 'peppy',
  salty: 'salty',
  hearty: 'hearty',
  zesty: 'zesty',
  refined: 'refined',
};

/**
 * @readonly
 * @enum {string}
 */
const TasteCold = {
  tasteless: 'tasteless',
  mild: 'mild',
  sweet: 'sweet',
  soft: 'soft',
  flakey: 'flakey',
  dry: 'dry',
};


const TechniqueRange = {
  melee: 'melee',
  touch: 'touch',
  ranged: 'ranged',
  reach: 'reach',
  reliable: 'reliable',
};


const StatType = {
  // hp: 'hp',
  melee: 'melee',
  armour: 'armour',
  ranged: 'ranged',
  dodge: 'dodge',
  speed: 'speed',
};


/**
 * @enum
 */
const StatusEffectType = {
  blinded: 'blinded',
  burn: 'burn',
  chargedup: 'chargedup',
  charging: 'charging',
  confused: 'confused',
  diehard: 'diehard',
  dozing: 'dozing',
  elementalshield: 'elementalshield',
  eliminated: 'eliminated',
  enraged: 'enraged',
  exhausted: 'exhausted',
  faint: 'faint',
  feedback: 'feedback',
  festering: 'festering',
  flinching: 'flinching',
  focused: 'focused',
  grabbed: 'grabbed',
  hardshell: 'hardshell',
  harpooned: 'harpooned',
  lifeleech: 'lifeleech',
  lockdown: 'lockdown',
  noddingoff: 'noddingoff',
  poison: 'poison',
  prickly: 'prickly',
  recover: 'recover',
  slow: 'slow',
  sniping: 'sniping',
  softened: 'softened',
  stuck: 'stuck',
  tired: 'tired',
  wasting: 'wasting',
  wild: 'wild',
};

const StatusEffectTypeColor = {
  [StatusEffectType.burn]: 'red',
  [StatusEffectType.poison]: 'purple',
  [StatusEffectType.recover]: 'white',
};


/**
 * @enum
 */
const EvolutionPathType = {
  standard: 'standard',
  item: 'item',
};

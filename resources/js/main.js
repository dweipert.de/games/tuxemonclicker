UI.wrapCallback(async function () {
  await initializeDB();

  // Load existing state
  if (localStorage.getItem('state')) {
    Memory.loadFromLocalStorage();
  }

  // Start New Game
  else {
    await initializeState();

    await Story.progress('start');
  }
})();

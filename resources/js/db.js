/**
 * @typedef {Object} DB_Evolution
 * @property {string} path
 * @property {string} monster_slug
 * @property {string} at_level
 * @property {string} item
 *
 * @typedef {Object} DB_NPC
 * @property {string}   slug
 * @property {Object[]} template
 * @property {string}   template[].sprite_name
 *
 * @typedef {Object} DB_Story
 * @property {number} money
 * @property {string} area
 * @property {boolean} canTriggerMultipleTimes
 * @property {Object<string, DB_StoryCharacter>} characters
 * @property {Object<string, DB_StoryEncounter>} encounters
 *
 * @typedef {Object} DB_StoryCharacter
 * @property {string}   slug
 * @property {string[]} text
 * @property {Object[]} monsters
 *
 * @typedef {Object} DB_StoryEncounter
 * @property {string} slug
 * @property {number} level
 */

//

const DB = {
  /**
   * @type {string[]}
   */
  allMonsters: [],

  /**
   * @typedef {Object.<string, string[]>} DB_Animation
   *
   * @type {DB_Animation}
   */
  allAnimations: {},

  /**
   * @type {string[]}
   */
  allItems: [],

  /**
   * @typedef {Object} DB_Monster
   *
   * @type {Object.<MonsterSlug, DB_Monster>}
   */
  monsters: {},

  /**
   * @typedef {Object} DB_Shape
   *
   * @type {Object.<string, DB_Shape>}
   */
  shapes: {},

  /**
   * @typedef {Object} DB_Element
   *
   * @type {Object.<ElementType, DB_Element>}
   */
  elements: {},

  /**
   * @typedef {Object} DB_Technique
   *
   * @type {Object.<TechniqueSlug, DB_Technique>}
   */
  techniques: {},

  /**
   * @typedef {Object} DB_StatusEffect
   *
   * @type {Object.<string, DB_StatusEffect>}
   */
  statusEffects: {},

  /**
   * @typedef {Object} DB_Item
   *
   * @type {Object.<ItemSlug, DB_Item>}
   */
  items: {},

  /**
   * @typedef {Object} DB_NPC
   *
   * @type {Object.<string, DB_NPC>}
   */
  npcs: {},

  /**
   * @typedef {Object} DB_Area
   *
   * @type {Object.<AreaSlug, DB_Area>}
   */
  areas: {},

  /**
   * @type {Object.<StorySlug, DB_Story>}
   */
  story: {},

  /**
   * @typedef {string} DB_Translation
   *
   * @type {Object.<string, DB_Translation>}
   */
  translations: {},

  currencies: {},
};

async function initializeDB () {
  DB.allMonsters = await fetchDBData('/db/_generated/all-monsters.json').then((response) => response.json());
  DB.allAnimations = await fetchDBData('/db/_generated/animations.json').then((response) => response.json());
  DB.allItems = await fetchDBData('/db/_generated/all-items.json').then((response) => response.json());

  DB.shapes = await fetchDBData('/modules/tuxemon/mods/tuxemon/db/shape/shapes.json').then((response) => response.json());

  for (const element of Object.keys(ElementType)) {
    DB.elements[element] = await fetchDBData(`/modules/tuxemon/mods/tuxemon/db/element/${element}.json`).then((response) => response.json());
  }

  await fetchTranslation('en_US'); // fallback
  await fetchTranslation(Memory.state.Settings.language);
  applyTranslation();

  DB.currencies = await fetchDBData('/db/_generated/currencies.json').then((response) => response.json());
}

/**
 * @param arguments
 *
 * @returns {Promise<Response>}
 */
async function fetchDBData (...arguments) {
  const response = await fetch(...arguments);

  if (!response.ok) {
    throw new Error(`"${response.url}": ${response.statusText}`);
  }

  return response;
}

/**
 * @param {MonsterSlug} slug
 *
 * @returns {Promise<Monster>}
 */
async function fetchMonster (slug) {
  if (! DB.monsters[slug]) {
    DB.monsters[slug] = await fetchDBData(`/modules/tuxemon/mods/tuxemon/db/monster/${slug}.json`).then((response) => response.json());
  }

  const monster = new Monster(slug);
  await monster.initialize();

  return monster;
}

/**
 * @param {TechniqueSlug} slug
 *
 * @returns {Promise<Technique>}
 */
async function fetchTechnique (slug) {
  if (! DB.techniques[slug]) {
    DB.techniques[slug] = await fetchDBData(`/modules/tuxemon/mods/tuxemon/db/technique/${slug}.json`).then((response) => response.json());
  }

  return new Technique(slug);
}

/**
 * @param {string} slug
 *
 * @returns {Promise<StatusEffect>}
 */
async function fetchStatusEffect (slug) {
  if (! DB.statusEffects[slug]) {
    DB.statusEffects[slug] = await fetchDBData(`/modules/tuxemon/mods/tuxemon/db/technique/status_${slug}.json`).then((response) => response.json());
  }

  return new StatusEffect(slug);
}

/**
 * @param {ItemSlug} slug
 *
 * @returns {Promise<Item>}
 */
async function fetchItem (slug) {
  if (! DB.items[slug]) {
    DB.items[slug] = await fetchDBData(`/modules/tuxemon/mods/tuxemon/db/item/${slug}.json`).then((response) => response.json());
  }

  return new Item(slug);
}

/**
 * @param {string} slug
 *
 * @returns {Promise<Npc>}
 */
async function fetchNpc (slug) {
  if (! DB.npcs[slug]) {
    DB.npcs[slug] = await fetchDBData(`/db/_generated/npc/${slug}.json`).then((response) => response.json());
  }

  return new Npc(slug);
}

/**
 * @param {StorySlug} slug
 *
 * @returns {Promise<DB_Story>}
 */
async function fetchStory (slug) {
  if (! DB.story[slug]) {
    DB.story[slug] = await fetchDBData(`/db/story/${slug}.json`).then((response) => response.json());
  }

  return DB.story[slug];
}

/**
 * @param {string} locale
 *
 * @returns {Promise<DB_Translation>}
 */
async function fetchTranslation (locale) {
  if (! DB.translations[locale]) {
    DB.translations[locale] = await fetchDBData(`/db/_generated/i18n/${locale}.json`).then((response) => response.json());
  }

  return DB.translations[locale];
}

/**
 * @param {AreaSlug} slug
 *
 * @returns {Promise<Area>}
 */
async function fetchArea (slug) {
  if (Memory.state.areaProgress[slug]) {
    return Memory.state.areaProgress[slug];
  }

  if (! DB.areas[slug]) {
    DB.areas[slug] = await fetchDBData(`/db/_generated/areas/${slug}.json`).then((response) => response.json());
  }

  const area = new Area(slug);

  return area;
}

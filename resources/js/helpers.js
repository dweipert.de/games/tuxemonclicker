/**
 * @param {string} slug
 *
 * @returns {(string|MonsterSlug|TechniqueSlug)}
 */
function slugToName (slug) {
  const ucfirst = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  return slug
    .split('_').map((item) => ucfirst(item)).join(' ')
    .split('-').map((item) => ucfirst(item)).join(' ');
}

/**
 * @param {string} text
 *
 * @returns {string}
 */
function nl2br (text) {
  return text.replace(new RegExp(/\\n/g), '<br><br>');
}

/**
 * @param {string} color
 *
 * @returns {string}
 */
function standardizeColor (color) {
  var ctx = document.createElement('canvas').getContext('2d');
  ctx.fillStyle = color;

  return ctx.fillStyle;
}

/**
 * @param {...string} colors
 *
 * @returns {string} rgb
 */
function mixColors(...colors) {
  let r = 0;
  let g = 0;
  let b = 0;

  for (const color of colors) {
    const [cr, cg, cb] = color.match(/\w\w/g).map((c) => parseInt(c, 16));

    r += cr;
    g += cg;
    b += cb;
  }

  r = r / colors.length;
  g = g / colors.length;
  b = b / colors.length;

  return `rgb(${r}, ${g}, ${b})`;
}

/**
 * @returns {string}
 */
function randomString () {
  return (Math.random() + 1).toString(36).substring(2);
}

/**
 * @param {string} msgid
 * @param {boolean} useFallback
 *
 * @returns {string}
 */
function translate (msgid, useFallback = true) {
  let translation = DB.translations[Memory.state.Settings.language][msgid];

  if (!translation && useFallback) {
    translation = DB.translations['en_US'][msgid];

    if (!translation) {
      translation = (translate('translation_missing', false) || 'translation_missing') + `: ${msgid}`;
    }
  }

  return translation;
}

function applyTranslation () {
  document.querySelectorAll('body, template').forEach((element) => {
    let parentNode = element;
    if (element.content) {
      parentNode = element.content;
    }

    parentNode.querySelectorAll('[data-i18n-msgid]').forEach((node) => {
      node.innerHTML = translate(node.dataset.i18nMsgid);
    });

    parentNode.querySelectorAll('[data-i18n-properties]').forEach((node) => {
      const properties = node.dataset.i18nProperties.split(',')
      const msgids = node.dataset.i18nPropertyMsgids.split(',')
      for (const idx in properties) {
        const property = properties[idx];
        const msgid = msgids[idx];

        node.setAttribute(property, translate(msgid))
      }
    });
  });
}

/**
 * @param {number} amount
 *
 * @returns {number}
 */
function convertToCurrencyBase (amount) {
  const baseDecimalDiff = 2 - DB.currencies.map[Memory.state.Settings.currency].decimals;

  return amount * Math.pow(10, baseDecimalDiff);
}

/**
 * @param {number} price
 *
 * @returns {number}
 */
function formatPrice (price) {
  return `${price} ${DB.currencies.map[Memory.state.Settings.currency].symbol}`;
}

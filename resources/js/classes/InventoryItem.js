class InventoryItem {
  /**
   * @type {Item}
   */
  item = null;

  /**
   * @type {number}
   */
  quantity = 1;

  /**
   * @param {Item} item
   */
  constructor (item, quantity = 1) {
    this.item = item;
    this.quantity = quantity;
  }

  /* Item */

  get slug () {
    return this.item.slug;
  }

  get name () {
    return this.item.name;
  }

  get description () {
    return this.item.description;
  }

  get category () {
    return this.item.category;
  }

  get type () {
    return this.item.type;
  }

  get sprite () {
    return this.item.sprite;
  }

  get conditions () {
    return this.item.conditions;
  }

  get effects () {
    return this.item.effects;
  }
}

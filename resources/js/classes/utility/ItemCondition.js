class ItemCondition {
  /**
   * @param {string} conditionCode
   */
  constructor (conditionCode) {
    this.is = conditionCode.split(' ')[0];
    this.what = conditionCode.split(' ')[1];

    if (conditionCode.includes(',')) {
      this.comparator = conditionCode.split(' ')[2].split(',')[0];
      this.value = conditionCode.split(' ')[2].split(',')[1];
    }

    else {
      this.value = conditionCode.split(' ')[2];
    }
  }
}

class ItemEffect {
  /**
   * @type {string}
   */
  type = '';

  /**
   * @type {('increase')}
   */
  application = '';

  /**
   * @type {number}
   */
  amount = 0;

  /**
   * @param {string} effectCode
   */
  constructor (effectCode) {
    if (effectCode.startsWith('heal')) {
      this.type = 'heal';

      this.amount = parseInt(effectCode.split(' ')[1]);
    }

    else if (effectCode.startsWith('revive')) {
      this.type = 'revive';

      this.amount = effectCode.split(' ')[1];
    }

    else if (effectCode.startsWith('learn_')) {
      this.type = effectCode.split(' ')[0];

      this.what = effectCode.split(' ')[1];
    }

    else if (Object.values(StatType).includes(effectCode.split(' ')[1] || '')) {
      this.type = 'stat';

      this.application = effectCode.split(' ')[0];
      this.what = effectCode.split(' ')[1].split(',')[0];
      this.amount = effectCode.split(' ')[1].split(',')[1];
    }

    else {
      this.type = effectCode;
    }
  }
}

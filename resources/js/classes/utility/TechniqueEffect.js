class TechniqueEffect {
  /**
   * @type {string}
   */
  type = '';

  /**
   * @type {('user' | 'target')}
   */
  recipient = null;

  /**
   * @type {Monster[]}
   */
  recipients = [];

  /**
   * @type {('give' | 'remove')}
   */
  application = '';

  /**
   * @type {StatusEffectType}
   */
  statusEffect = null;

  /**
   * @type {ElementType}
   */
  switchType = '';

  /**
   * @type {Monster}
   */
  user = null;

  /**
   * @type {Monster}
   */
  target = null;

  /**
   * @param {string} effectCode
   */
  constructor (effectCode) {
    if (effectCode.includes('status_')) {
      this.type = 'status';

      this.recipient = effectCode.split(',')[1];
      this.application = effectCode.split(' ')[0];
      this.statusEffect = effectCode.split(',')[0].split(' ')[1].split('_')[1];
    }

    else if (effectCode.includes('healing')) {
      this.type = 'healing';

      this.recipient = effectCode.split(' ')[1];
    }

    else if (effectCode.startsWith('switch')) {
      this.type = 'switch';

      this.recipient = effectCode.split(' ')[1].split(',')[0];
      this.switchType = effectCode.split(' ')[1].split(',')[1];
    }

    else {
      this.type = effectCode;
    }
  }

  /**
   * @type {Monster}
   */
  setUser (user) {
    if (['user', 'both'].includes(this.recipient)) {
      this.recipients.push(user);
    }
  }

  /**
   * @type {Monster}
   */
  setTarget (target) {
    if (['target', 'both'].includes(this.recipient)) {
      this.recipients.push(target);
    }
  }
}

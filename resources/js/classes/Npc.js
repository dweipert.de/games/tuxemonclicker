class Npc {
  name = '';
  spriteName = '';

  constructor (slug) {
    this.slug = slug;

    this.name = translate(slug);
    this.spriteName = DB.npcs[slug]?.template[0].combat_front;
  }

  get sprite () {
    return `/modules/tuxemon/mods/tuxemon/gfx/sprites/player/${this.spriteName}.png`;
  }
}

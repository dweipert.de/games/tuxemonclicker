class State {
  Settings = {
    name: '',

    /**
     * @type {string}
     */
    language: 'en_US',

    currency: 'EUR',

    colors: {
      player: '#00ff00',
      player: '#ff0000',
    },

    logMaxLength: 1000,
  };

  Game = {
    isInBattle: false,
  };

  /**
   * @type {Object.<string, Area>}
   */
  areaProgress = {};

  /**
   * @type {Area}
   */
  currentArea = null;

  /**
   * @type {AreaSlug}
   */
  lastVisitedTown = '';

  /**
   * @type {Object.<string, boolean>}
   */
  storyProgress = {};

  /**
   * @type {string}
   */
  currentStory = null;

  /**
   * @type {number}
   */
  turn = 0;

  /**
   * @type {number}
   */
  money = 0;

  /**
   * @type {Monster[]}
   */
  monsters = [];

  /**
   * @type {Trainer}
   */
  player = null;

  /**
   * @type {Trainer}
   */
  opponent = null;

  /**
   * @type {MonsterSlug}
   */
  rivalMonster = '';

  /**
   * @type {Technique}
   */
  activeTechnique = null;

  /**
   * @type {ItemSlug}
   */
  activeBall = '';
};

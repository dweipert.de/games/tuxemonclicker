class StatusEffect {
  turnsLeft = 0;
  onRemove = null;

  /**
   * @type {Monster}
   *
   * currently only used for lifeleech
   */
  issuer = null;

  constructor (slug) {
    this.slug = slug;

    if (['recover', 'lifeleech'].includes(this.slug)) {
      this.turnsLeft = 1;
    }
    else if (['charging'].includes(this.slug)) {
      this.turnsLeft = 2;
    }
    else if (['faint'].includes(this.slug)) {
      this.turnsLeft = Number.MAX_SAFE_INTEGER;
    }
    else if (this.category === 'positive') {
      this.turnsLeft = Math.ceil(Math.random() * 6) + 4;
    }
    else if (this.category === 'negative') {
      this.turnsLeft = Math.ceil(Math.random() * 3) + 2;
    }
    else {
      this.turnsLeft = Math.ceil(Math.random() * 3) + 2;
    }
  }

  /**
   * @returns {string[]}
   */
  get effects () {
    return DB.statusEffects[this.slug].effects;
  }

  get category () {
    return DB.statusEffects[this.slug].category;
  }

  get name () {
    return translate(`status_${this.slug}`);
  }

  get stats () {
    const stats = {};

    const statsChangeKeys = Object.keys(DB.statusEffects[this.slug]).filter((key) => key.startsWith('stat'));
    for (const statChangeKey of statsChangeKeys) {
      stats[statChangeKey.replace('stat', '')] = DB.statusEffects[this.slug][statChangeKey];
    }

    return stats;
  }
}

class Trainer {
  #monsters = [];
  #inventory = [];

  /**
   * @type {('trainer' | 'monster')}
   */
  type = 'trainer';

  name = '';
  sprite = '';

  /**
   * @type {Monster[]}
   */
  monsters = [];

  /**
   * @type {InventoryItem[]}
   */
  inventory = [];

  /**
   * @type {Monster}
   */
  activeMonster = [];

  /**
   * @param {(Monster[]|Object[])} monsters
   * @param {(InventoryItem[]|Object[])} inventory
   */
  constructor (trainerData) {
    this.#monsters = trainerData.monsters;
    this.#inventory = trainerData.inventory || this.inventory;
    this.type = trainerData.type || this.type;
    this.name = trainerData.name;
    this.sprite = trainerData.sprite;
  }

  async initialize () {
    for (const monsterData of this.#monsters) {
      if (monsterData instanceof Monster) {
        this.monsters.push(monsterData);
      } else {
        const monster = await fetchMonster(monsterData.slug);

        monster.level = monsterData.level || monster.level;

        this.monsters.push(monster);
      }
    }

    this.activeMonster = this.monsters[0];

    for (const itemData of this.#inventory) {
      if (itemData instanceof InventoryItem) {
        this.inventory.push(itemData);
      } else {
        const item = new InventoryItem(await fetchItem(itemData.slug));

        item.quantity = itemData.quantity || 1;
        this.inventory.push(item);
      }
    }
  }
}

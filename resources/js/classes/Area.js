class Area {
  slug = '';

  monsterProgress = 0;
  trainerProgress = 0;

  constructor (slug) {
    this.slug = slug;
  }

  get name () {
    return translate(this.alternateSlug);
  }

  get isCompleted () {
    return this.monsterProgress >= this.requiredEncounters && this.trainerProgress >= this.trainers.length;
  }

  get alternateSlug () {
    return DB.areas[this.slug]['modules/tuxemon.slug'];
  }

  /**
   * @returns {Object[]}
   */
  get encounters () {
    return DB.areas[this.slug].encounters;
  }

  get encounterPercentTotal () {
    return DB.areas[this.slug].encounter_percent_total;
  }

  get requiredEncountersBase () {
    return DB.areas[this.slug].requiredEncounters;
  }

  get requiredEncounters () {
    return this.requiredEncountersBase * (this.trainerProgress + 1);
  }

  get trainers () {
    return DB.areas[this.slug].trainers;
  }

  /**
   * @typedef {Object} AreaItem
   * @property {ItemSlug} slug
   * @property {number}   dropRatio
   * @inner
   *
   * @returns {AreaItem[]}
   */
  get items () {
    return DB.areas[this.slug].items;
  }

  get environment () {
    return DB.areas[this.slug].environment;
  }

  get map () {
    return DB.areas[this.slug].map;
  }

  /**
   * @returns {Object[]}
   */
  get locations () {
    return DB.areas[this.slug].locations;
  }

  get events () {
    return DB.areas[this.slug].events;
  }

  get connections () {
    return DB.areas[this.slug].connections;
  }
}

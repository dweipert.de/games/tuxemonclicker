class Technique {
  #accuracy = 0;
  #potency = 0;
  #power = 0;

  turnLastUse = 0;

  constructor (slug) {
    this.slug = slug;

    this.resetStats();

    this.turnLastUse = -this.rechargeLength;
  }

  get name () {
    return translate(this.slug);
  }

  get description () {
    return translate(`${this.slug}_description`);
  }

  get types () {
    return DB.techniques[this.slug].types;
  }

  get range () {
    return DB.techniques[this.slug].range;
  }

  get animation () {
    return DB.techniques[this.slug].animation;
  }

  get sfx () {
    return DB.techniques[this.slug].sfx;
  }

  get healingPower() {
    return DB.techniques[this.slug].healing_power;
  }

  /**
   * @returns {string[]}
   */
  get effects () {
    return DB.techniques[this.slug].effects;
  }

  get rechargeLength () {
    return DB.techniques[this.slug].recharge;
  }

  isRecharging () {
    if (this.turnLastUse >= Memory.state.turn) {
      return false;
    }

    return this.rechargeLength > Memory.state.turn - this.turnLastUse;
  }

  use () {
    this.turnLastUse = Memory.state.turn;
  }

  get accuracy () {
    return this.#accuracy;
  }
  set accuracy (accuracy) {
    this.#accuracy = accuracy;
  }

  get potency () {
    return this.#potency;
  }
  set potency (potency) {
    this.#potency = potency;
  }

  get power () {
    return this.#power;
  }
  set power (power) {
    this.#power = power;
  }

  get stats () {
    const accuracy = DB.techniques[this.slug].accuracy;
    const potency = DB.techniques[this.slug].potency;
    const power = DB.techniques[this.slug].power;

    return {
      accuracy,
      potency,
      power,
    };
  }

  resetStats () {
    this.accuracy = this.stats.accuracy;
    this.potency = this.stats.potency;
    this.power = this.stats.power;
  }
}

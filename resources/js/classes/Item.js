class Item {
  constructor (slug) {
    this.slug = slug;
  }

  get name () {
    return translate(this.slug);
  }

  get description () {
    return translate(`${this.slug}_description`);
  }

  get category () {
    return DB.items[this.slug].category;
  }

  get type () {
    return DB.items[this.slug].type;
  }

  get sprite () {
    const sprite = DB.items[this.slug].sprite;

    if (sprite) {
      return '/modules/tuxemon/mods/tuxemon/' + sprite;
    }

    return '';
  }

  get conditions () {
    return DB.items[this.slug].conditions;
  }

  get effects () {
    return DB.items[this.slug].effects;
  }
}

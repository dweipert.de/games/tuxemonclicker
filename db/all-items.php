<?php

$allItems = [];
foreach (scandir(dirname(__DIR__) . '/modules/tuxemon/mods/tuxemon/db/item') as $file) {
  if (in_array($file, ['.', '..'])) continue;

  $allItems[] = pathinfo($file, PATHINFO_FILENAME);
}

file_put_contents(__DIR__ . '/_generated/all-items.json', json_encode($allItems));

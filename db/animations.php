<?php

$animations = [];
foreach (scandir(dirname(__DIR__) . '/modules/tuxemon/mods/tuxemon/animations/technique') as $file) {
  if (in_array($file, ['.', '..'])) continue;

  $animationFileName = pathinfo($file, PATHINFO_FILENAME);
  $animationName = substr($animationFileName, 0, -3);

  $animations[$animationName][] = $animationFileName;
}

file_put_contents(__DIR__ . '/_generated/animations.json', json_encode($animations));

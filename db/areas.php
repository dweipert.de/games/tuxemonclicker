<?php

foreach (scandir(__DIR__ . '/areas') as $file) {
  if (in_array($file, ['.', '..'])) continue;

  $filePath = __DIR__ . '/areas/' . $file;
  $fileName = pathinfo($file, PATHINFO_FILENAME);

  $area = json_decode(file_get_contents($filePath), true);


  // encounters

  $area['encounters'] ??= [];

  $encounterSlug = $area['modules/tuxemon.encounter'] ?? '';
  $encounters = json_decode(@file_get_contents(dirname(__DIR__) . "/modules/tuxemon/mods/tuxemon/db/encounter/$encounterSlug.json") ?? '', true);
  array_push($area['encounters'], ...$encounters['monsters'] ?? []);

  if (! empty($area['encounters'])) {
    // filter out duplicates, because day/night is ignored for now
    $duplicates = [];
    $area['encounters'] = array_values(array_filter($area['encounters'], function ($item) use (&$duplicates) {
      $isNotDuplicate = ! in_array($item['monster'], $duplicates);
      $duplicates[] = $item['monster'];
      return $isNotDuplicate;
    }));

    $encounterRateTotal = array_sum(array_column($area['encounters'], 'encounter_rate'));
    foreach ($area['encounters'] as &$encounter) {
      if (empty($encounter['encounter_rate'])) continue;
      $encounter['encounter_percent'] = intval(100 / ($encounterRateTotal / $encounter['encounter_rate']));
    }
    $area['encounter_percent_total'] = array_sum(array_column($area['encounters'], 'encounter_percent'));
  }

  $area['requiredEncounters'] ??= 0;


  // trainers

  $area['trainers'] ??= [];


  // items

  $area['items'] ??= [];


  // environment

  $environmentSlug = $area['modules/tuxemon.environment'] ?? '';
  $environment = json_decode(@file_get_contents(dirname(__DIR__) . "/modules/tuxemon/mods/tuxemon/db/environment/$environmentSlug.json") ?? '', true);

  if (isset($area['environment'])) {
    $environment = json_decode(file_get_contents(__DIR__ . "/environment/$area[environment].json"), true);
    $environment['battle_graphics']['background'] = '/db/environment/' . $environment['battle_graphics']['background'];
  } else if (! empty($environment)) {
    $environment['battle_graphics']['background'] = '/modules/tuxemon/mods/tuxemon/gfx/ui/combat/' . $environment['battle_graphics']['background'];
  }

  $area['environment'] = $environment;


  // map

  $map = @file_get_contents(__DIR__ . "/maps/$fileName.svg");

  $area['map'] = $map;


  // locations

  $area['locations'] ??= [];
  foreach ($area['locations'] as $locationId => $location) {
    if ($location['type'] == 'shop') {
      $economySlug = $location['modules/tuxemon.economy'];
      $scoop = json_decode(file_get_contents(dirname(__DIR__) . "/modules/tuxemon/mods/tuxemon/db/economy/$economySlug.json"), true);
      array_unshift($area['locations'][$locationId]['items'], ...$scoop['items']);
    }
  }


  // connections

  foreach ($area['connections'] as $areaSlug => $connection) {
    $area['connections'][$areaSlug]['modules/tuxemon.slug'] = json_decode(@file_get_contents(__DIR__ . "/areas/$areaSlug.json") ?? '', true)['modules/tuxemon.slug'] ?? '';
  }


  // build

  file_put_contents(__DIR__ . "/_generated/areas/$fileName.json", json_encode($area));
}

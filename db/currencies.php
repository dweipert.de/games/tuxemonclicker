<?php

$currencySymbols = [];
$handle = fopen('https://raw.githubusercontent.com/bengourley/currency-symbol-map/master/map.js', 'r');
while ($line = fgets($handle)) {
  preg_match("/(\w+): '(.+)'/", $line, $match);

  if (isset($match[1])) {
    $currencySymbols[$match[1]] = $match[2];
  }
}

$currencyNames = json_decode(file_get_contents('https://openexchangerates.org/api/currencies.json'), true);

$decimals = [
  'JPY' => 0,
  'KRW' => 0,
];

$currencies = [];
$apiResponse = json_decode(file_get_contents('https://api.frankfurter.app/latest?base=eur'), true);
$currencies['last_updated'] = $apiResponse['date'];
foreach ($apiResponse['rates'] as $code => $rate) {
  $currencies['map'][$code] = [
    'rate' => $rate,
    'symbol' => $currencySymbols[$code],
    'name' => $currencyNames[$code],
    'decimals' => $decimals[$code] ?? 2,
  ];
}

// add EUR
$currencies['map']['EUR'] = [
  'rate' => 1,
  'symbol' => $currencySymbols['EUR'],
  'name' => $currencyNames['EUR'],
  'decimals' => 2,
];

file_put_contents(
  __DIR__ . '/_generated/currencies.json',
  json_encode($currencies)
);

<?php

$allMonsters = [];
foreach (scandir(dirname(__DIR__) . '/modules/tuxemon/mods/tuxemon/db/monster') as $file) {
  if (in_array($file, ['.', '..'])) continue;

  $allMonsters[] = pathinfo($file, PATHINFO_FILENAME);
}

file_put_contents(__DIR__ . '/_generated/all-monsters.json', json_encode($allMonsters));

<?php

global $i18n;
$i18n = [];
function populateI18n ($locale, $poFile) {
  global $i18n;
  $i18n[$locale] ??= [];

  $handle = fopen($poFile, 'r');
  while ($line = fgets($handle)) {
    if (str_starts_with($line, 'msgid')) {
      $nextLine = fgets($handle);
      preg_match('/"(.*)"/', $line, $msgidMatch);
      preg_match('/"(.*)"/', $nextLine, $msgstrMatch);

      if (empty($msgidMatch[1]) || str_starts_with($msgidMatch[1], 'PROJECT DESCRIPTION')) continue;
      $msgid = str_replace('\n', '', trim($msgidMatch[1]));

      // get lines immediately after msgstr
      $msgstr = $msgstrMatch[1];
      $msgstrNextLine = fgets($handle);
      if (str_starts_with($msgstrNextLine, 'msgid')) {
        fseek($handle, -strlen($msgstrNextLine), SEEK_CUR);
      } else {
        while (! empty(trim($msgstrNextLine))) {
          preg_match('/"(.*)"/', $msgstrNextLine, $msgstrNextLineMatch);
          $msgstr .= $msgstrNextLineMatch[1];

          $msgstrNextLine = fgets($handle);
        }
      }

      $i18n[$locale][$msgid] = mb_convert_encoding($msgstr, 'UTF-8');
    }
  }
}

// tuxemon clicker
$basePath = __DIR__ . '/i18n';
foreach (scandir($basePath) as $file) {
  if (in_array($file, ['.', '..'])) continue;

  $locale = pathinfo($file, PATHINFO_FILENAME);
  $poFile = "$basePath/$file";
  populateI18n($locale, $poFile);
}

// modules/tuxemon
$basePath = dirname(__DIR__) . '/modules/tuxemon/mods/tuxemon/l18n';
foreach (scandir($basePath) as $dirname) {
  if (in_array($dirname, ['.', '..']) || ! is_dir("$basePath/$dirname")) continue;

  $poFile = "$basePath/$dirname/LC_MESSAGES/base.po";
  populateI18n($dirname, $poFile);
}

// write
foreach ($i18n as $locale => $translations) {
  file_put_contents(__DIR__ . "/_generated/i18n/$locale.json", json_encode($translations));
}

<?php

$npcs = [];

// modules/tuxemon
$basePath = dirname(__DIR__) . '/modules/tuxemon/mods/tuxemon/db/npc';
foreach (scandir($basePath) as $fileName) {
  if (in_array($fileName, ['.', '..'])) continue;

  $filePath = "$basePath/$fileName";
  $json = json_decode(file_get_contents($filePath), true);

  if (isset($json['slug']) && isset($json['template'])) {
    $npcs[$json['slug']] = $json;
  } else {
    foreach ($json as $npc) {
      $npcs[$npc['slug']] = $npc;
    }
  }
}

// tuxemon clicker
$basePath = __DIR__ . '/npc';
foreach (scandir($basePath) as $fileName) {
  if (in_array($fileName, ['.', '..'])) continue;

  $filePath = "$basePath/$fileName";
  $npcData = json_decode(file_get_contents($filePath), true);

  $slug = $npcData['modules/tuxemon.slug'] ?? pathinfo($fileName, PATHINFO_FILENAME);
  $npc = [
    'slug' => $slug,
    'template' => [
      [
        'sprite_name' => $npcData['sprite'],
        'combat_front' => $npcData['sprite'],
      ],
    ],
  ];

  if (isset($npcs[$slug])) {
    $npcs[$slug] = array_replace_recursive($npcs[$slug], $npc);
  } else {
    $npcs[$slug] = $npc;
  }
}

foreach ($npcs as $slug => $npc) {
  file_put_contents(__DIR__ . "/_generated/npc/$slug.json", json_encode($npc));
}
